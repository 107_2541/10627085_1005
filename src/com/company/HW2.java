package com.company;

import java.util.Scanner;

public class HW2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a;

        System.out.print("Please Enter Number : ");
        a = sc.nextInt();
        if ((a & 1) == 0) {
            System.out.println(a + "是偶數 ");
        } else {
            System.out.println(a + "是奇數 ");
        }
    }
}